package garage;

public class Bycicle extends Vehicle{

  public boolean pedal()
  {
      if(isMoving)
      {
          return true;
      }
      return false;
  }

    public Bycicle(String colour, int numberOfWheels) {
        super(colour, numberOfWheels);
    }

    @Override
    public boolean isMoving() {
        return false;
    }

    @Override
    public boolean Move() {
        return false;
    }

    @Override
    public boolean Stop() {
        return false;
    }
}
